"""
This is a sample hello world flask app
It only has a root resource which sends back hello World html text
"""
from __future__ import division, print_function, unicode_literals

import json
import re
import numpy as np
import tensorflow as tf
import faiss
from faiss import normalize_L2
from flask import Flask, request
import configparser
import os
import nltk
import csv

nltk.download('punkt')
nltk.download('stopwords')

from nltk import sent_tokenize
from autoencoder import TextAutoencoder
from utils import text_prepare
from xpresso.ai.core.data.inference import AbstractInferenceService
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "Shlok Chaudhari"

logger = XprLogger("irrs_infer")
app = Flask(__name__)


class IrrsInfer(AbstractInferenceService):
    """ Main class for the inference service. User will need to implement
    following functions:
       - load_model: It gets the directory of the model stored as parameters,
           user will need to implement the method for loading the model and
           updating self.model variable
       - transform_input: It gets the JSON object from the rest API as input.
           User will need to implement the method to convert the json data to
           relevant feature vector to be used for prediction
       - predict: It gets the feature vector or any other object from the
           transform_input method. User will need to implement the model
           prediction codebase here. It returns the predicted value
       - transform_output: It gets the predicted value from the predict method.
           User need to implement this method to converted predicted method
           to JSON serializable object. Response from this method is send back
           to the client as JSON Object.

    AbstractInferenceService automatically creates the flask reset api
    with resource /predict for this class.
    Request JSON format:
       {
         "input" : <input_goes_here> // It could be any JSON object
       }
       This value of "input" key is sent to transform_input

    Response JSON format:
       {
         "message": "success/failure",
         "results": <response goes here> // It could be any JSOn object
       }
       Output of transform_output goes as value of "results"
    """

    def __init__(self):
        super().__init__()
        """ Initialize any static data required during boot up """
        self.path = os.getcwd()
        config = configparser.RawConfigParser()
        configFilePath = './config/config.properties'
        config.read(configFilePath)
        print(configFilePath)
        self.server = config.get('ENDPOINT', 'server')
        self.port = config.get('ENDPOINT', 'port1')
        self.embedding_size = 2 * int(config.get('AutoEncoder', 'lstm_units'))
        # get title embeddings , vocabulary and model from config
        self.model_path = config.get('ENCODER', 'model_path')
        self.vocabulary = config.get('ENCODER', 'vocabulary')
        self.title_embedding_path = config.get('ENCODER', 'title_embedding')
        self.desc_embedding_path = self.path + '/' + config.get('ENCODER', 'desc_embedding')
        self.sess = 0
        self.app = 0
        self.desc_index = faiss.IndexFlatIP(self.embedding_size)
        self.title_index = faiss.IndexFlatIP(self.embedding_size)
        self.complete_desc_id_list = []
        self.title_id_list = []
        self.ticket_details = {}
        self.sess = tf.InteractiveSession()
        self.model = TextAutoencoder.load(self.model_path, self.sess)
        logger.info('loaded title model')
        self.process_title_section()
        self.process_desc_section()
        self.load_desc_embeddings()
        self.load_title_embeddings()
        self.load_incident_details()

    def process_title_section(self):
        fields = ['sent', 'id']
        with open(self.path + '/data/datadrive/title_to_id_mapping_preprocessed.csv', 'r') as f:
            reader = csv.DictReader(f, fieldnames=fields)
            for row in reader:
                self.title_id_list.append(row['id'])
            f.close()
        logger.info('loaded title mapping')

    def process_desc_section(self):
        fields = ['sent', 'id']
        with open(self.path + '/data/datadrive/desc_to_id_mapping_preprocessed.csv', 'r') as f:
            reader = csv.DictReader(f, fieldnames=fields)
            for row in reader:
                self.complete_desc_id_list.append(row['id'])
            f.close()
        logger.info('loaded desc embeddings')

    def load_desc_embeddings(self):
        filenames = os.listdir(self.desc_embedding_path)
        sorted_filenames = sorted(filenames)

        for i in range(0, len(sorted_filenames) - 1):
            filename = sorted_filenames[i]
            if filename.endswith(".npy"):
                embedding_path = self.desc_embedding_path + '/' + filename
                embedding = np.load(embedding_path)
                normalize_L2(embedding)
                self.desc_index.add(embedding)
                del embedding

        logger.info('loaded desc mapping')

    def load_title_embeddings(self):
        for filename in os.listdir(self.title_embedding_path):
            if filename.endswith(".npy"):
                embedding_path = self.title_embedding_path + '/' + filename
                embedding = np.load(embedding_path)
                normalize_L2(embedding)
                self.title_index.add(embedding)
                del embedding

        logger.info('loaded title to vec maping')

    def load_incident_details(self):
        fields = ['id', 'details']
        with open(self.path + '/data/datadrive/incident_details.csv', 'r') as f:
            reader = csv.DictReader(f, fieldnames=fields)
            for row in reader:
                self.ticket_details[row['id']] = row['details']
            f.close()

        logger.info('incident details loaded')

    @staticmethod
    def prepare_text(s):
        lines = s.split('\n ')
        sents = []
        print(lines)
        for line in lines:
            new_sents = sent_tokenize(line)
            for sent in new_sents:
                sents.append(text_prepare(sent))
        return sents

    def get_sentence_embedding(self, line, vocabulary, model):
        sent = []
        sent.append(line)
        wd = utils.WordDictionary(vocabulary)
        sentences, sizes = utils.load_text_data_list(sent, wd)
        num_sents = 1
        next_index = 0
        all_states = []
        while next_index < len(sentences):
            batch = sentences[next_index:next_index + num_sents]
            batch_sizes = sizes[next_index:next_index + num_sents]
            next_index += num_sents
            state = model.encode(self.sess, batch, batch_sizes)
            all_states.append(state)

        state = np.vstack(all_states)
        return state

    class MyEncoder(json.JSONEncoder):
        def default(self, obj):
            if isinstance(obj, np.integer):
                return int(obj)
            elif isinstance(obj, np.floating):
                return float(obj)
            elif isinstance(obj, np.ndarray):
                return obj.tolist()
            else:
                return super(self.MyEncoder, self).default(obj)

    @staticmethod
    def result_by_count(ticket_list):
        tkt_count_map = {}
        for sample in ticket_list:
            tkt_id = sample[0]
            if tkt_id in tkt_count_map.keys():
                tkt_count_map[tkt_id] = tkt_count_map[tkt_id] + 1
            else:
                tkt_count_map[tkt_id] = 1
        tkt_score_map = {}
        for sample in ticket_list:
            tkt_id = sample[0]
            score = sample[1]
            if tkt_id in tkt_score_map.keys():
                current_score = tkt_score_map[tkt_id]
                if score < current_score:
                    tkt_score_map[tkt_id] = score
            else:
                tkt_score_map[tkt_id] = score
        cnt_tkt_map = {}
        for k, v in tkt_count_map.items():
            if v in cnt_tkt_map:
                count_list = cnt_tkt_map[v]
                count_list.append((k, tkt_score_map[k]))
                cnt_tkt_map[v] = count_list
            else:
                cnt_tkt_map[v] = [(k, tkt_score_map[k])]
        tkt_count_list = sorted(cnt_tkt_map.keys(), reverse=True)
        final_list = []
        i = 0
        while i < len(tkt_count_list) and len(final_list) < 50:
            cur_count = tkt_count_list[i]

            cur_count_list = cnt_tkt_map[cur_count]

            cur_count_list.sort(key=lambda x: x[1], reverse=True)

            for item in cur_count_list:
                final_list.append(item)
                if len(final_list) >= 50:
                    break
            i += 1
        return final_list

    @staticmethod
    def line_valid(line, wd):
        words = line.split()
        count = 0
        for w in words:
            if w in wd:
                count += 1
        if count >= 2:
            return 1
        else:
            return 0

    def find_closest_desc(self, sent, vocabulary, model):
        result = []
        lines = self.prepare_text(sent)
        wd = utils.WordDictionary(vocabulary)
        count = 0
        for line in lines:
            line = re.sub('_', ' ', line)

            valid = self.line_valid(line, wd)
            if valid == 1:

                vec1 = self.get_sentence_embedding(line, vocabulary, model)[0]
                if count == 0:
                    vec = np.reshape(vec1, (1, 512))
                    normalize_L2(vec)
                    count += 1
                else:
                    vec1 = np.reshape(vec1, (1, 512))
                    normalize_L2(vec1)
                    vec = np.append(vec, vec1, axis=0)

        D, I = self.desc_index.search(vec, 100)
        result = []
        for i in range(0, len(D)):
            for j in range(0, len(D[0])):
                result.append((I[i][j], D[i][j]))

        final_result = self.result_by_count(result)
        return final_result

    def prepare_result(self, data, flag):
        final_result = []
        for sample in data:
            tkt_idx = sample[0]
            score = sample[1]
            if flag == 0:
                tkt_id = self.complete_desc_id_list[tkt_idx]
            else:
                if tkt_idx >= len(self.title_id_list):
                    continue
                tkt_id = self.title_id_list[tkt_idx]
            tkt_details = self.ticket_details[tkt_id]
            details = {}
            details['incident'] = tkt_id
            details['score'] = np.float32(score)
            details['Title & Desc'] = tkt_details
            final_result.append(details)
        return final_result

    def find_closest_titles(self, sent, vocabulary, model):
        result = []
        lines = self.prepare_text(sent)
        print(lines)
        wd = utils.WordDictionary(vocabulary)
        count = 0
        print(vocabulary)
        for line in lines:
            line = re.sub('_', ' ', line)
            print(line)
            valid = self.line_valid(line, wd)
            if valid == 1:
                print(line)
                vec1 = self.get_sentence_embedding(line, vocabulary, model)[0]
                if count == 0:
                    vec = np.reshape(vec1, (1, 512))
                    normalize_L2(vec)
                    count += 1
                else:
                    vec1 = np.reshape(vec1, (1, 512))
                    normalize_L2(vec1)
                    vec = np.append(vec, vec1, axis=0)
        if count == 0:
            final_result = []
        else:
            D, I = self.title_index.search(vec, 100)
            print(D)
            print(I)
            result = []
            for i in range(0, len(D)):
                for j in range(0, len(D[0])):
                    result.append((I[i][j], D[i][j]))

            final_result = self.result_by_count(result)
        return final_result

    @staticmethod
    def get_merged_ticket_list(ticket_list):
        ticket_score_map = {}
        for t in ticket_list:
            tkt_id = t[0]
            score = t[1]
            if tkt_id in ticket_score_map.keys():
                if ticket_score_map[tkt_id] < score:
                    ticket_score_map[tkt_id] = score
            else:
                ticket_score_map[tkt_id] = score
        sorted_ticket_map = sorted(ticket_score_map.items(), key=lambda kv: kv[1])
        return sorted_ticket_map[0:10]

    @app.route('/find_ticket', methods=['Post'])
    def find_tickets(self):
        content = request.get_json()
        title = content['title']
        desc = content['description']

        title = re.sub(r'\\n', '\n', title)
        desc = re.sub(r'\\n', '\n', desc)
        print(title)
        if len(title) > 2 and len(desc) > 5:
            msg = 'ok'
            result1 = self.find_closest_titles(title, self.vocabulary, self.model)
            final_result1 = self.prepare_result(result1, 1)
            result2 = self.find_closest_desc(desc, self.vocabulary, self.model)
            final_result2 = self.prepare_result(result2, 0)
            final_result = final_result1[0:5]
            final_result.extend(final_result2[0:5])
            dictionary = {'result': final_result, 'messages': msg}
        elif len(title) > 2:
            print("finaind title")
            msg = 'ok'
            result = self.find_closest_titles(title, self.vocabulary, self.model)
            print(result)
            final_result = self.prepare_result(result, 1)
            dictionary = {'result': final_result, 'messages': msg}

        elif len(desc) > 5:
            msg = 'ok'
            result = self.find_closest_desc(desc, self.vocabulary, self.model)
            final_result = self.prepare_result(result, 0)
            dictionary = {'result': final_result, 'messages': msg}
        else:
            msg = 'no valid title &  desc'
            dictionary = {'result': [], 'messages': msg}
            return json.dumps(dictionary, cls=self.MyEncoder)

        return json.dumps(dictionary, cls=self.MyEncoder)

    def load_model(self, model_path):
        """ This is used to load the model on the boot time.
        User will need to load the model and save it in the variable
        self.model. It is IMPORTANT to update self.model.
        Args:
            model_path(str): Path of the directory where the model files are
               stored.
        """
        app.run(host=self.server, port=self.port, debug=True, threaded=False)

    def transform_input(self, input_request):
        """
        Convert the raw input request to a feature data or any other object
        to be used during prediction

        Args:
            input_request: JSON Object or an array received from the REST API,
               which needs to be converted into relevant feature data.

        Returns:
            obj: Any transformed object
        """
        pass

    def predict(self, input_request):
        """
        This method implements the prediction method of the supported model.
        It gets the output of transform_input as input and returns the predicted
        value

        Args:
            input_request: Transformed object outputted from transform_input
               method

        Returns:
            obj: predicted value from the model

        """
        pass

    def transform_output(self, output_response):
        """
        Convert the predicted value into a relevant JSON serializable object.
        This is sent back to the REST API call
        Args:
            output_response: Predicted output from predict method.

        Returns:
          obj: JSON Serializable object
        """
        pass


if __name__ == "__main__":
    pred = IrrsInfer()
    # === To run locally. Use load_model instead of load. ===
    # pred.load_model(model_path="/model_path/") instead of pred.load()
    pred.load()
    pred.run_api(port=5000)
