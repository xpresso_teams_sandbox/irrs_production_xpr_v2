"""
This is the implementation of data preparation for sklearn
"""

import configparser
import math
import os
import re
import sys
import csv
import ast
from nltk import sent_tokenize
from collections import defaultdict, Counter

import nltk
import numpy as np
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from nltk.corpus import stopwords

nltk.download('stopwords')
nltk.download('punkt')

from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "Shlok Chaudhari"

logger = XprLogger("irrs_data_prep")


class IrrsDataPrep(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self):
        super().__init__(name="IrrsDataPrep")
        """ Initialize all the required constansts and data her """

    @staticmethod
    def text_prepare(text):
        REPLACE_BY_SPACE_RE = re.compile('[/(){}\[\]\|,;0123456789-]#:"_')
        BAD_SYMBOLS_RE = re.compile('[^0-9a-zA-Z #+_]')
        single = '\s[a-z]{1}\s'
        stopwords1 = ['this', 'that', 'This', 'That', 'I', 'where', 'Where', 'there', 'There']
        STOPWORDS = set(stopwords.words('english'))
        text = text.lower()
        text = re.sub('\n+', '\n', text)
        text = re.sub('\t', ' ', text)
        text = re.sub(REPLACE_BY_SPACE_RE, ' ', text)
        text = re.sub(BAD_SYMBOLS_RE, ' ', text)
        text = re.sub(single, ' ', text)
        text = ' '.join([word for word in text.split() if word not in STOPWORDS])
        text = ' '.join([word for word in text.split() if word not in stopwords1])
        text = re.sub(r'\b\d+\b', ' ', text)
        text = re.sub(r"'", ' ', text)
        text = ' '.join(text.split())
        text = text.lstrip()
        text = text.rstrip()
        return text

    def create_data(self):

        config = configparser.RawConfigParser()
        configFilePath = 'config/config.properties'
        config.read(configFilePath)
        logger.info(configFilePath)
        starspace_training_data = []
        dataxlsx = config.get('DATAPATH', 'dataxlsx')
        datadrive = config.get('DATAPATH', 'datadrive')
        datapath = dataxlsx
        datadrive = datadrive

        def get_dataframe(data):
            columns = []
            df = pd.DataFrame(columns)
            # = os.fspath(data)
            flag = 0
            filenames = os.listdir(data)
            sorted_filenames = sorted(filenames)
            for filename in sorted_filenames:
                if filename.endswith(".xlsx"):
                    logger.info("filename =  {}".format(filename))
                    path = os.path.join(data, filename)
                    reader = pd.ExcelFile(path)
                    if flag == 0:
                        if (len(reader.sheet_names) == 1):
                            df = reader.parse(reader.sheet_names[0])
                        else:
                            df = reader.parse(reader.sheet_names[1])
                        flag = 1
                    else:
                        if (len(reader.sheet_names) == 1):
                            new_df = reader.parse(reader.sheet_names[0])
                        else:
                            new_df = reader.parse(reader.sheet_names[1])
                        new_df.columns = df.columns
                        df = df.append(new_df)
            return df

        df = get_dataframe(datapath)

        id_list = df['INCIDENT_ID'].tolist()
        title_list = df['TITLE'].tolist()
        desc_list = df['DESCRIPTION'].tolist()

        ticket_details_map = {}
        for i in range(0, len(title_list)):
            title = title_list[i]
            desc = desc_list[i]
            tkt_id = id_list[i]
            ticket_details_map[tkt_id] = {'title': title, 'description': desc}

        filename = datadrive + '/incident_details.csv'
        dirname = os.path.dirname(filename)

        if not os.path.exists(dirname):
            os.makedirs(dirname)

        fields = ['id', 'details']
        with open(filename, 'w+') as f:
            writer = csv.DictWriter(f, fieldnames=fields)
            for k, v in ticket_details_map.items():
                writer.writerow({'id': k, 'details': v})
            f.close()

        logger.info("created file =  {}".format(filename))

        fields = ['id', 'details']

        # generate title related files
        title_list = []

        with open(datadrive + '/incident_details.csv', 'r') as f1:
            reader = csv.DictReader(f1, fieldnames=fields)
            for row in reader:
                details = ast.literal_eval(row['details'])
                text = self.text_prepare(details['title'])
                sentences = text.split('\n')
                for sent in sentences:

                    words = sent.split()
                    if len(words) > 2:
                        title_list.append(sent)
            f1.close()

        v = CountVectorizer(min_df=0.00001, max_df=1.0)

        v.fit(desc_list)

        vocab_set = set(v.get_feature_names())
        complete_words = set()

        for line in title_list:
            words = line.split()
            for word in words:
                complete_words.add(word)

        stopwords = complete_words - vocab_set

        def prepare_text(text):
            line = ' '.join([word for word in text.split() if word not in stopwords])
            return line

        fields2 = ['sent', 'id']

        filename = datadrive + '/title_to_id_mapping_without_preprocessing.csv'
        with open(datadrive + '/incident_details.csv', 'r') as f1:
            reader = csv.DictReader(f1, fieldnames=fields)
            with open(filename, 'w+') as f2:
                writer = csv.DictWriter(f2, fieldnames=fields2)
                for row in reader:
                    details = ast.literal_eval(row['details'])
                    text = details['title']
                    sentences = text.split('\n')
                    for sents in sentences:
                        lines = sent_tokenize(sents)
                        for line in lines:
                            if len(line.split()) > 1:
                                writer.writerow({'sent': line.encode("utf-8"), 'id': row['id']})

                f2.close()
            f1.close()

        logger.info("created file =  {}".format(filename))

        title_training_list = []
        filename = datadrive + '/title_to_id_mapping_preprocessed.csv'
        with open(datadrive + '/incident_details.csv', 'r') as f1:
            reader = csv.DictReader(f1, fieldnames=fields)
            with open(filename, 'w+') as f2:
                writer = csv.DictWriter(f2, fieldnames=fields2)
                for row in reader:
                    details = ast.literal_eval(row['details'])
                    text = details['title']
                    sentences = text.split('\n')
                    complete_sentence = ''
                    for sents in sentences:
                        lines = sent_tokenize(sents)

                        for line in lines:
                            new_sent = self.text_prepare(line)
                            new_sent = prepare_text(new_sent)
                            new_sent = re.sub('_', ' ', new_sent)
                            words = new_sent.split()
                            if len(words) > 1:
                                writer.writerow({'sent': new_sent, 'id': row['id']})
                                title_training_list.append(new_sent)
                                complete_sentence = complete_sentence + '\t' + new_sent
                    complete_sentence = complete_sentence.strip()
                    starspace_training_data.append(complete_sentence)
                f2.close()
            f1.close()

        logger.info("created file =  {}".format(filename))

        title_unique_set = set()
        filename = datadrive + '/title_data_splitted.txt'
        with open(filename, 'a') as f:
            for sample in title_training_list:
                f.write(sample + '\n')
                title_unique_set.add(sample)
            f.close()

        logger.info("created file =  {}".format(filename))

        filename = datadrive + '/title_data_training.txt'
        with open(filename, 'a') as f:
            for sample in title_unique_set:
                f.write(sample + '\n')
            f.close()

        logger.info("created file =  {}".format(filename))

        # generate desc related files

        desc_list = []

        with open(datadrive + '/incident_details.csv', 'r') as f1:
            reader = csv.DictReader(f1, fieldnames=fields)
            for row in reader:
                details = ast.literal_eval(row['details'])
                text = self.text_prepare(details['description'])
                sentences = text.split('\n')
                for sent in sentences:

                    words = sent.split()
                    if len(words) > 2:
                        desc_list.append(sent)
            f1.close()

        v = CountVectorizer(min_df=0.00001, max_df=1.0)

        v.fit(desc_list)

        vocab_set = set(v.get_feature_names())
        complete_words = set()

        for line in desc_list:
            words = line.split()
            for word in words:
                complete_words.add(word)

        stopwords = complete_words - vocab_set

        filename = datadrive + '/desc_to_id_mapping_without_preprocessing.csv'
        with open(datadrive + '/incident_details.csv', 'r') as f1:
            reader = csv.DictReader(f1, fieldnames=fields)
            with open(filename, 'a') as f2:
                writer = csv.DictWriter(f2, fieldnames=fields2)
                for row in reader:
                    details = ast.literal_eval(row['details'])
                    text = details['description']
                    sentences = text.split('\n')
                    for sents in sentences:
                        lines = sent_tokenize(sents)
                        for line in lines:
                            if len(line.split()) > 1:
                                writer.writerow({'sent': line.encode("utf-8"), 'id': row['id']})

                f2.close()
            f1.close()

        logger.info("created file =  {}".format(filename))

        desc_training_list = []
        filename = datadrive + '/desc_to_id_mapping_preprocessed.csv'
        with open(datadrive + '/incident_details.csv', 'r') as f1:
            reader = csv.DictReader(f1, fieldnames=fields)
            with open(filename, 'a') as f2:
                writer = csv.DictWriter(f2, fieldnames=fields2)
                for row in reader:
                    details = ast.literal_eval(row['details'])
                    text = details['description']
                    sentences = text.split('\n')
                    complete_sentence = ''
                    for sents in sentences:
                        lines = sent_tokenize(sents)

                        for line in lines:
                            new_sent = self.text_prepare(line)
                            new_sent = prepare_text(new_sent)
                            new_sent = re.sub('_', ' ', new_sent)
                            words = new_sent.split()
                            if len(words) > 1:
                                writer.writerow({'sent': new_sent, 'id': row['id']})
                                desc_training_list.append(new_sent)
                                complete_sentence = complete_sentence + '\t' + new_sent
                    complete_sentence = complete_sentence.strip()
                    starspace_training_data.append(complete_sentence)
                f2.close()
            f1.close()

        logger.info("created file =  {}".format(filename))

        desc_unique_set = set()
        filename = datadrive + '/desc_data_splitted.txt'
        with open(filename, 'a') as f:
            for sample in desc_training_list:
                f.write(sample + '\n')
                desc_unique_set.add(sample)
            f.close()

        logger.info("created file =  {}".format(filename))

        filename = datadrive + '/desc_data_training.txt'
        with open(filename, 'a') as f:
            for sample in desc_unique_set:
                f.write(sample + '\n')
            f.close()
        logger.info("created file =  {}".format(filename))

        combined_set = title_unique_set.union(desc_unique_set)

        filename = datadrive + '/combined_data_training.txt'
        with open(filename, 'a') as f:
            for sample in combined_set:
                f.write(sample + '\n')
            f.close()

        logger.info("created file =  {}".format(filename))

        filename = datadrive + '/starspace_training.txt'
        with open(filename, 'a') as f:
            for sample in starspace_training_data:
                f.write(sample + '\n')
            f.close()
        logger.info("created file =  {}".format(filename))

    def load_data_memory_friendly(self, path, max_size, min_occurrences=10,
                                  valid_proportion=0.01):
        """
        Return a tuple (dict, dict, list) where each dict maps names to
        sentence matrices and sizes arrays (first is train, second is validation);
        the list is the vocabulary
        """
        token_counter = Counter()
        size_counter = Counter()

        # first pass to build vocabulary and count sentence sizes
        logger.info('Creating vocabulary...')
        with open(path, 'rb') as f:
            for line in f:
                line = line.decode('utf-8')
                tokens = line.split()
                sent_size = len(tokens)
                if sent_size > max_size:
                    continue

                # keep track of different size bins, with bins for
                # 1-10, 11-20, 21-30, etc
                top_bin = int(math.ceil(sent_size / 10) * 10)
                size_counter[top_bin] += 1
                token_counter.update(tokens)

        # sort it keeping the order
        vocabulary = [w for w, count in token_counter.most_common()
                      if count >= min_occurrences]
        # this might break the ordering, but hopefully is not a problem
        vocabulary.insert(0, '</s>')
        vocabulary.insert(1, '<unk>')
        word_dict = {}
        for i in range(len(vocabulary)):
            word_dict[vocabulary[i]] = i
        dd = defaultdict(int, word_dict)

        # now read the corpus again to fill the sentence matrix
        logger.info('Converting word to indices...')
        train_data = {}  # dictionary to be used with numpy.savez
        valid_data = {}
        for threshold in size_counter:
            min_threshold = threshold - 9
            num_sentences = size_counter[threshold]
            logger.info('Converting %d sentences with length between %d and %d'
                        % (num_sentences, min_threshold, threshold))
            sents, sizes = self.create_sentence_matrix(path, num_sentences,
                                                       min_threshold, threshold, dd)

            # shuffle sentences and sizes with the sime RNG state
            state = np.random.get_state()
            np.random.shuffle(sents)
            np.random.set_state(state)
            np.random.shuffle(sizes)

            ind = int(len(sents) * valid_proportion)
            valid_sentences = sents[:ind]
            valid_sizes = sizes[:ind]
            train_sentences = sents[ind:]
            train_sizes = sizes[ind:]

            train_data['sentences-%d' % threshold] = train_sentences
            train_data['sizes-%d' % threshold] = train_sizes
            valid_data['sentences-%d' % threshold] = valid_sentences
            valid_data['sizes-%d' % threshold] = valid_sizes

        logger.info('Numeric representation ready')
        return train_data, valid_data, vocabulary

    @staticmethod
    def create_sentence_matrix(path, num_sentences, min_size,
                               max_size, word_dict):
        """
        Create a sentence matrix from the file in the given path.
        :param path: path to text file
        :param min_size: minimum sentence length, inclusive
        :param max_size: maximum sentence length, inclusive
        :param num_sentences: number of sentences expected
        :param word_dict: mapping of words to indices
        :return: tuple (2-d matrix, 1-d array) with sentences and
            sizes
        """
        sentence_matrix = np.full((num_sentences, max_size), 0, np.int32)
        sizes = np.empty(num_sentences, np.int32)
        i = 0
        with open(path, 'rb') as f:
            for line in f:
                line = line.decode('utf-8')
                tokens = line.split()
                sent_size = len(tokens)
                if sent_size < min_size or sent_size > max_size:
                    continue

                array = np.array([word_dict[token] for token in tokens])
                sentence_matrix[i, :sent_size] = array
                sizes[i] = sent_size
                i += 1

        return sentence_matrix, sizes

    @staticmethod
    def load_data(path, max_size, min_occurrences=10):
        """
        Load data from a text file and creates the numpy arrays
        used by the autoencoder.

        :return: a tuple (sentences, sizes, vocabulary).
            sentences is a 2-d matrix padded with EOS
            sizes is a 1-d array with each sentence size
            vocabulary is a list of words positioned according to their indices
        """
        sentences = []
        sizes = []
        longest_sent_size = 0
        index = [0]  # hack -- use a mutable object to be

        # accessed inside the nested function
        # at first, 0 means padding/EOS

        def on_new_word():
            index[0] += 1
            return index[0]

        word_dict = defaultdict(on_new_word)

        with open(path, 'rb') as f:
            for line in f:
                line = line.decode('utf-8')
                tokens = line.split()
                sent_size = len(tokens)
                if sent_size > max_size:
                    continue
                sentences.append([word_dict[token]
                                  for token in tokens])
                sizes.append(sent_size)
                if sent_size > longest_sent_size:
                    longest_sent_size = sent_size

        reverse_word_dict = {v: k for k, v in word_dict.items()}
        reverse_word_dict[0] = '</s>'
        # we initialize the matrix now that we know the number of sentences
        sentence_matrix = np.full((len(sentences), longest_sent_size),
                                  0, np.int32)

        for i, sent in enumerate(sentences):
            sentence_array = np.array(sent)
            sentence_matrix[i, :sizes[i]] = sentence_array

        # count occurrences of tokens on the remaining sentences
        # counter: index -> num_occurences
        counter = Counter(sentence_matrix.flat)

        # 0 signs the EOS token, it should be counted once per sentence
        counter[0] = len(sentence_matrix)

        # these words will be replaced by the unk token
        unk_words = [(w, counter[w]) for w in counter
                     if counter[w] < min_occurrences]
        unk_count = sum(item[1] for item in unk_words)
        unk_index = len(counter)  # make the unknown index the last one
        counter[unk_index] = unk_count
        reverse_word_dict[unk_index] = '<unk>'

        # now we sort word indices by frequency (this works better with some
        # sampling techniques such as Noise Constrastive Estimation)
        replacements = {}
        word_list = []
        for new_index, (old_index, count) in enumerate(counter.most_common()):
            if count < min_occurrences:
                # we can break the loop because the next ones
                # have equal or lower counts
                break

            replacements[old_index] = new_index
            word_list.append(reverse_word_dict[old_index])

        new_unk_index = replacements[unk_index]
        replacements_with_unk = defaultdict(lambda: new_unk_index,
                                            replacements)
        original_shape = sentence_matrix.shape
        replaced = np.array([replacements_with_unk[w]
                             for w in sentence_matrix.flat],
                            dtype=np.int32)
        sentence_matrix = replaced.reshape(original_shape)

        sizes_array = np.array(sizes, dtype=np.int32)
        return sentence_matrix, sizes_array, word_list

    @staticmethod
    def write_vocabulary(words, path):
        """
        Write the contents of word_dict to the given path.
        """
        text = '\n'.join(words)
        with open(path, 'wb') as f:
            f.write(text.encode('utf-8'))

    def prepare_data(self):
        config = configparser.RawConfigParser()
        configFilePath = './config/config.properties'
        config.read(configFilePath)
        logger.info(configFilePath)
        datadrive = config.get('DATAPATH', 'datadrive')
        input_path = datadrive + '/combined_data_training.txt'
        output_path = config.get('PREPROCESS', 'output')
        max_length = int(config.get('PREPROCESS', 'max_length'))
        min_freq = int(config.get('PREPROCESS', 'min_freq'))
        valid_prop = float(config.get('PREPROCESS', 'valid_prop'))

        train_data, valid_data, words = self.load_data_memory_friendly(
            input_path, max_length, min_freq, valid_prop)
        logger.info(output_path)
        output_path = output_path

        if not os.path.exists(output_path):
            logger.info('directory does not exist')
            os.makedirs(output_path)
        logger.info(output_path)
        if os.path.exists(output_path):
            logger.info('directory exist now')
        valid_path = output_path + '/valid-data.npz'
        with open(valid_path, 'w+') as f:
            np.savez(valid_path, **valid_data)
            f.close()
        validation_data = np.load(valid_path)
        logger.info(valid_path)
        train_path = output_path + '/train-data.npz'
        with open(train_path, 'w+') as f:
            np.savez(train_path, **train_data)
            f.close()

        vocab_path = output_path + '/vocabulary.txt'
        with open(vocab_path, 'w+') as f:
            self.write_vocabulary(words, vocab_path)
            f.close()

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        super().start(xpresso_run_name=run_name)
        # === Your start code base goes here ===
        self.create_data()
        self.completed()

    def send_metrics(self):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        report_status = {
            "status": {"status": "irrs_data_prepare component complete"},
            "metric": {"metric_key": 1}
        }
        self.report_status(status=report_status)

    def completed(self, push_exp=False):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned

        """
        # === Your start code base goes here ===
        self.prepare_data()
        self.send_metrics()
        super().completed(push_exp=False)

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        super().terminate()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        super().pause()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        super().restart()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/app.py

    data_prep = IrrsDataPrep()
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
