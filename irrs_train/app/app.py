"""
This is the implementation of data preparation for sklearn
"""

import sys
import tensorflow as tf
import numpy as np
from utils import WordDictionary, load_binary_data
from autoencoder import TextAutoencoder
from gensim.models import KeyedVectors
import configparser
import os

from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "Shlok Chaudhari"

logger = XprLogger("irrs_train")


class IrrsTrain(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self):
        super().__init__(name="IrrsTrain")
        """ Initialize all the required constansts and data her """
        logger.info(os.getcwd())
        logger.info(os.path.dirname(os.path.abspath(__file__)))
        config = configparser.RawConfigParser()
        configFilePath = 'config/config.properties'
        config.read(configFilePath)
        logger.info(configFilePath)
        self.model_path = config.get('AutoEncoder', 'model_path')
        logger.info(self.model_path)
        self.train = config.get('AutoEncoder', 'train')
        self.valid = config.get('AutoEncoder', 'valid')
        self.vocab = config.get('AutoEncoder', 'vocab')
        self.epoch = int(config.get('AutoEncoder', 'epoch'))
        self.embedding_size = int(config.get('AutoEncoder', 'embedding_size'))
        self.lstm_units = int(config.get('AutoEncoder', 'lstm_units'))
        self.batch_size = int(config.get('AutoEncoder', 'batch_size'))
        self.learning_rate = float(config.get('AutoEncoder', 'learning_rate'))
        self.dropout = float(config.get('AutoEncoder', 'dropout'))
        self.interval = int(config.get('AutoEncoder', 'interval'))
        if not os.path.exists(self.model_path):
            logger.info('directory does not exist')
            os.makedirs(self.model_path)
        if os.path.exists(self.model_path):
            logger.info('directory exist now')

    @staticmethod
    def show_parameter_count(variables):
        """
        Count and logger.info how many parameters there are.
        """
        total_parameters = 0
        for variable in variables:
            name = variable.name

            # shape is an array of tf.Dimension
            shape = variable.get_shape()
            variable_parametes = 1
            for dim in shape:
                variable_parametes *= dim.value
            logger.info('{}: {} ({} parameters)'.format(name,
                                                        shape,
                                                        variable_parametes))
            total_parameters += variable_parametes

        logger.info('Total: {} parameters'.format(total_parameters))

    @staticmethod
    def load_or_create_embeddings(path, wd, embedding_size):
        """
        If path is given, load an embeddings file. If not, create a random
        embedding matrix with shape (vocab_size, embedding_size)
        """
        if path is not None:
            model = KeyedVectors.load_word2vec_format(path)
            words = wd.d.keys()
            len(words)
            vec_words = []
            for word in model.vocab:
                vec_words.append(word)
            embedding = np.array([])
            count = 0
            for word in words:
                if word in vec_words:
                    embedding = np.append(embedding, model[word])
                    count += 1
                else:
                    vec = np.random.uniform(-5, 5, (1, 300))
                    embedding = np.append(embedding, vec)
            logger.info(count)
            embedding = embedding.reshape(len(wd.d), 300)
            return embedding.astype(np.float32)

        embeddings = np.random.uniform(-0.1, 0.1, (len(wd.d), embedding_size))
        return embeddings.astype(np.float32)

    def training_models(self):
        sess = tf.Session()
        wd = WordDictionary(self.vocab)
        embeddings = self.load_or_create_embeddings(None, wd,
                                                    self.embedding_size)

        logger.info('Reading training data')
        train_data = load_binary_data('./' + self.train)
        logger.info('Reading validation data')
        valid_data = load_binary_data('./' + self.valid)
        logger.info('Creating model')

        train_embeddings = True
        model = TextAutoencoder(self.lstm_units,
                                embeddings, wd.eos_index,
                                train_embeddings=train_embeddings,
                                bidirectional=True)

        sess.run(tf.global_variables_initializer())
        self.show_parameter_count(model.get_trainable_variables())
        logger.info('Initialized the model and all variables. Starting training.')
        model.train(sess, self.model_path, train_data, valid_data, self.batch_size,
                    self.epoch, self.learning_rate,
                    self.dropout, 5.0, report_interval=self.interval)

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        super().start(xpresso_run_name=run_name)
        # === Your start code base goes here ===
        self.training_models()
        self.completed()

    def send_metrics(self):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        report_status = {
            "status": {"status": "data_preparation"},
            "metric": {"metric_key": 1}
        }
        self.report_status(status=report_status)

    def completed(self, push_exp=False):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned

        """
        # === Your start code base goes here ===
        super().completed(push_exp=False)

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        super().terminate()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        super().pause()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        super().restart()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/app.py

    data_prep = IrrsTrain()
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
