"""
This is the implementation of data preparation for sklearn
"""

import sys
import json
import os
import configparser
import numpy as np
import tensorflow as tf
import utils
from autoencoder import TextAutoencoder

from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "Shlok Chaudhari"

logger = XprLogger("irrs_codify")


class IrrsCodify(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self):
        super().__init__(name="IrrsCodify")
        """ Initialize all the required constansts and data her """
        config = configparser.RawConfigParser()
        configFilePath = 'config/config.properties'
        config.read(configFilePath)
        logger.info(configFilePath)
        self.model_path = config.get('CODIFY', 'model_path')
        self.title_data = config.get('CODIFY', 'title_data')
        self.desc_data = config.get('CODIFY', 'desc_data')
        self.vocab = config.get('CODIFY', 'vocab')
        self.title_output_path = config.get('CODIFY', 'title_path')
        self.desc_output_path = config.get('CODIFY', 'desc_path')
        self.num_sents = 500

    @staticmethod
    def load_model(directory, embeddings, session):
        model_path = os.path.join(directory, 'model')
        metadata_path = os.path.join(directory, 'metadata.json')
        with open(metadata_path, 'r') as f:
            metadata = json.load(f)
        dummy_embeddings = embeddings
        ae = TextAutoencoder(metadata['num_units'], dummy_embeddings,
                             metadata['go'], train=False,
                             bidirectional=metadata['bidirectional'])
        vars_to_load = ae.get_trainable_variables()
        vars_to_load.remove(ae.embeddings)
        saver = tf.train.Saver(vars_to_load)
        saver.restore(session, model_path)
        return ae

    def create_embeddings(self, input_datapath, output_datapath, model, sess, wd):
        # generate desc embeddings
        datapath = input_datapath
        with open(datapath, 'r') as f:
            complete_sentences = f.readlines()

        count = 0
        if len(complete_sentences) <= 800000:
            number_of_chunks = 1
        else:
            number_of_chunks = int(len(complete_sentences) / 800000)
        logger.info("number of title chunks = " + str(number_of_chunks))

        sentences = []
        if len(complete_sentences) < 800000:
            sentences.append(complete_sentences)
        else:
            p = 0
            q = 800000
            for i in range(0, number_of_chunks):
                sentences.append(complete_sentences[p:q])
                p = q
                q = q + 800000

        output_path = output_datapath

        if not os.path.exists(output_path):
            logger.info('directory does not exist')
            os.makedirs(output_path)
        logger.info(output_path)
        if os.path.exists(self.desc_output_path):
            logger.info('directory exist now')
        # logger.info(sentences)
        for i in range(0, len(sentences)):
            transformed_sentences, sizes = utils.load_text_data_list(sentences[i], wd)
            embedding = output_path + '/embedding' + str(i) + '.npy'
            logger.info(embedding)
            next_index = 0
            all_states = []
            while next_index < len(transformed_sentences):
                logger.info(count)
                batch = transformed_sentences[next_index:next_index + self.num_sents]
                batch_sizes = sizes[next_index:next_index + self.num_sents]
                next_index += self.num_sents
                state = model.encode(sess, batch, batch_sizes)
                all_states.append(state)
                count += 1
            state = np.vstack(all_states)
            np.save(embedding, state)

    def codify(self):
        wd = utils.WordDictionary(self.vocab)
        logger.info('vocab loaded')
        m = self.model_path
        sess = tf.InteractiveSession()
        model = TextAutoencoder.load(m, sess)
        # generate title embedding
        self.create_embeddings(self.title_data, self.title_output_path, model, sess, wd)
        self.create_embeddings(self.desc_data, self.desc_output_path, model, sess, wd)

    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        super().start(xpresso_run_name=run_name)
        # === Your start code base goes here ===
        self.codify()
        self.completed()

    def send_metrics(self):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        report_status = {
            "status": {"status": "data_preparation"},
            "metric": {"metric_key": 1}
        }
        self.report_status(status=report_status)

    def completed(self, push_exp=False):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned

        """
        # === Your start code base goes here ===
        super().completed(push_exp=False)

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        super().terminate()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        super().pause()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        super().restart()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/app.py

    data_prep = IrrsCodify()
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
